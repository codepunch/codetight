package com.example.deliview.interfaces

import com.example.deliview.model.Delivery
import java.text.FieldPosition

interface DeliveryClickListener {

    fun onItemClickListener(delivery: Delivery, position: Int)
}