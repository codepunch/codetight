package com.example.deliview.activity

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.deliview.R
import com.example.deliview.adapter.DeliveryAdapter
import com.example.deliview.model.Delivery
import com.example.deliview.mvp.contract.DeliveryInteractor
import com.example.deliview.mvp.presenter.DeliveryPresenter
import com.example.deliview.mvp.view.DeliveryView
import com.example.deliview.utils.CommonUtils
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*

class DeliveryListActivity : AppCompatActivity(), DeliveryView {

    private lateinit var mDeliveryAdapter: DeliveryAdapter
    private val deliveryList = ArrayList<Delivery>()
    private lateinit var presenter: DeliveryInteractor
    private var etLimit: EditText? = null
    private var etOffset: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpToolbar()
        presenter = DeliveryPresenter(this)
        initializeRecyclerView()
        toggleNoItems(true)
        if (CommonUtils.isConnectedToNetwork(this))
            showRemoteSearchDialog()
        else
            presenter.getDeliveryItemsLocal()
    }


    private fun initializeRecyclerView() {
        mDeliveryAdapter = DeliveryAdapter(this, deliveryList)
        rv_deliveries.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_deliveries.itemAnimator = DefaultItemAnimator()
        // rv_deliveries.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        rv_deliveries.adapter = mDeliveryAdapter
    }

    override fun onFailureDeliveryFetch(e: Throwable) {
        showError(e)
    }

    override fun onSuccessDeliveryFetch(deliveryList: List<Delivery>?, isRemote: Boolean) {
        if (deliveryList?.size == 0) {
            toggleNoItems(true)
        } else {
            if (isRemote)
                presenter.clearAndInsertTable(deliveryList!!)
            toggleNoItems(false)
            this.deliveryList.clear()
            this.deliveryList.addAll(deliveryList!!)
            mDeliveryAdapter.notifyDataSetChanged()

        }
    }

    private fun setUpToolbar() {
        supportActionBar?.title = getString(R.string.delivery_list_title)
    }

    private fun showError(e: Throwable) {
        var message = ""
        try {
            if (e is IOException) {
                message = "No internet connection!"
            } else if (e is HttpException) {
                val error: HttpException = e
                val errorBody: String? = error.response().errorBody()?.string()
                val jObj = JSONObject(errorBody)

                message = jObj.getString("error");
            }
        } catch (e1: IOException) {
            e1.printStackTrace()
        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e1: Exception) {
            e1.printStackTrace()
        }


        if (TextUtils.isEmpty(message)) {
            message = "Unknown error occurred! Check LogCat."
        }

        val snackbar = Snackbar
                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG)

        val sbView = snackbar.view
        val textView = sbView.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        textView.setTextColor(Color.YELLOW)
        snackbar.show()
    }

    private fun showRemoteSearchDialog() {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_fetch, null)
        val builder = AlertDialog.Builder(this)
        builder.setView(dialogView)

        etLimit = dialogView.findViewById(R.id.et_limit)
        etOffset = dialogView.findViewById(R.id.et_offset)

        builder.setCancelable(false)
                .setPositiveButton(getString(R.string.ok_text), null)
                .setNegativeButton(getString(R.string.cancel_text), null)
        val searchDialog = builder.create()
        searchDialog.setOnShowListener { it ->
            val positiveButton = searchDialog.getButton(AlertDialog.BUTTON_POSITIVE)
            positiveButton.setOnClickListener {
                val limit = etLimit!!.text
                val offset = etOffset!!.text
                if (limit.isEmpty() || offset.isEmpty()) {
                    Toast.makeText(this@DeliveryListActivity, getString(R.string.invalid_input), Toast.LENGTH_SHORT).show()
                } else {
                    presenter.getDeliveryItemsRemote(Integer.parseInt(offset.toString()), Integer.parseInt(limit.toString()))
                    searchDialog.dismiss()
                }
                val negativeButton = searchDialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                negativeButton.setOnClickListener {
                    searchDialog.dismiss()
                }
            }
        }
        searchDialog.show()
    }

    private fun toggleNoItems(noItems: Boolean) {
        if (noItems) {
            rv_deliveries.visibility = View.GONE
            tv_no_items_available.visibility = View.VISIBLE
        } else {
            rv_deliveries.visibility = View.VISIBLE
            tv_no_items_available.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onStop()
    }
}
