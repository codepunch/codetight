package com.example.deliview.mvp.presenter

import com.example.deliview.application.DeliViewApplication
import com.example.deliview.model.Delivery
import com.example.deliview.mvp.contract.DeliveryInteractor
import com.example.deliview.mvp.view.DeliveryView
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.util.*


class DeliveryPresenter : DeliveryInteractor {

    private var deliveryView: DeliveryView
    private var compositeDisposable: CompositeDisposable

    constructor(deliveryView: DeliveryView) {
        this.deliveryView = deliveryView
        compositeDisposable = CompositeDisposable()
    }

    override fun getDeliveryItemsRemote(offset: Int, limit: Int) {
        compositeDisposable.add(
                DeliViewApplication.getRetrofitInstance()
                        .getDeliveries(offset, limit).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map { t ->
                            Collections.sort(t, object : Comparator<Delivery> {
                                override fun compare(n1: Delivery, n2: Delivery): Int {
                                    return n2.id!! - n1.id!!
                                }
                            })
                            t
                        }
                        .subscribeWith(object : DisposableSingleObserver<List<Delivery>>() {
                            override fun onSuccess(t: List<Delivery>) {
                                deliveryView.onSuccessDeliveryFetch(t, true)
                            }

                            override fun onError(e: Throwable) {
                                deliveryView.onFailureDeliveryFetch(e)
                            }

                        })
        )
    }

    override fun getDeliveryItemsLocal() {
        compositeDisposable.add(
                DeliViewApplication.getDatabaseInstance().deliveryDao().getAllDeliveries().subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map { t ->
                            Collections.sort(t) { n1, n2 -> n2.id!! - n1.id!! }
                            t
                        }
                        .subscribeWith(
                                object : DisposableSingleObserver<List<Delivery>>() {
                                    override fun onSuccess(t: List<Delivery>) {
                                        deliveryView.onSuccessDeliveryFetch(t, false)
                                    }

                                    override fun onError(e: Throwable) {
                                        deliveryView.onFailureDeliveryFetch(e)
                                    }

                                })
        )
    }

    override fun clearAndInsertTable(deliveryList: List<Delivery>) {
        val deliveryDao = DeliViewApplication.getDatabaseInstance().deliveryDao()
        compositeDisposable.add(
                Completable.fromAction {
                    deliveryDao.deleteAllRecords()
                }.subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
                        .concatWith(Completable.fromAction { deliveryDao.insertDelivery(deliveryList) }
                                .subscribeOn(Schedulers.io())
                                .observeOn(Schedulers.io())).subscribeWith(object : DisposableCompletableObserver() {
                            override fun onComplete() {
                            }

                            override fun onError(e: Throwable) {
                                deliveryView.onFailureDeliveryFetch(e)
                            }

                        })

        )
    }

    override fun onStop() {
        compositeDisposable.dispose()
    }
}