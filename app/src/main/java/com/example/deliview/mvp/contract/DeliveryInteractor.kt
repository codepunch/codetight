package com.example.deliview.mvp.contract

import com.example.deliview.model.Delivery

interface DeliveryInteractor {

    fun getDeliveryItemsRemote(offset:Int,limit:Int)

    fun getDeliveryItemsLocal()

    fun clearAndInsertTable(deliveryList: List<Delivery>)

    fun onStop()
}