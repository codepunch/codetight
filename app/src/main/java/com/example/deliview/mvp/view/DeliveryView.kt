package com.example.deliview.mvp.view

import com.example.deliview.model.Delivery

interface DeliveryView {

    fun onSuccessDeliveryFetch(deliveryList: List<Delivery>?, isRemote: Boolean)

    fun onFailureDeliveryFetch(e: Throwable)


}