package com.example.deliview.network

import com.example.deliview.constants.AppConstants
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class ApiClient {

    companion object {
        private var retrofit: Retrofit? = null
        private const val REQUEST_TIMEOUT: Long = 60
        private var okHttClient: OkHttpClient? = null

        fun getClient(): Retrofit {
            if (okHttClient == null)
                initOkHttpClient()
            if (retrofit == null) {
                retrofit = Retrofit.Builder().baseUrl(AppConstants.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(okHttClient!!)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()
            }
            return retrofit!!
        }

        private fun initOkHttpClient() {
            okHttClient = OkHttpClient.Builder().connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS).build()
        }

    }
}