package com.example.deliview.network

import com.example.deliview.model.Delivery
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/deliveries")
    fun getDeliveries(@Query("offset") offset: Int, @Query("limit") limit: Int): Single<List<Delivery>>
}