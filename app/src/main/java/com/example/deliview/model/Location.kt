package com.example.deliview.model

import java.io.Serializable

data class Location(
        var lat: Double? = 0.0,
        var lng: Double? = 0.0,
        var address: String? = null
) : Serializable