package com.example.deliview.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import com.example.deliview.data.LocationConverter
import java.io.Serializable

@Entity(tableName = "DeliveryData")
data class Delivery(
        @PrimaryKey(autoGenerate = false)
        var id: Int? = 0,
        @ColumnInfo(name = "description")
        var description: String? = null,
        @ColumnInfo(name = "imageUrl")
        var imageUrl: String? = null,
        @TypeConverters(LocationConverter::class)
        @ColumnInfo(name = "location")
        var location: Location? = null
) : Serializable {

    constructor() : this(0, "", "", null)
}