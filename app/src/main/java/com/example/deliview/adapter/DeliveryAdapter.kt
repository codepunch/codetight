package com.example.deliview.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.deliview.R
import com.example.deliview.R.id.iv_person_image
import com.example.deliview.activity.DeliveryMapActivity
import com.example.deliview.model.Delivery
import kotlinx.android.synthetic.main.bottom_sheet_delivery.*
import kotlinx.android.synthetic.main.item_delivery.view.*
import java.io.Serializable

class DeliveryAdapter(private val context: Context, private val deliveryList: List<Delivery>) : RecyclerView.Adapter<DeliveryAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        return CustomViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_delivery, parent, false))
    }

    override fun getItemCount(): Int {
        return deliveryList.size
    }


    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bindItems(deliveryList[position])
    }


    inner class CustomViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(delivery: Delivery) {
            itemView.setOnClickListener {
                val intent = Intent(context, DeliveryMapActivity::class.java)
                intent.putExtra(context.getString(R.string.delivery_item_key), delivery as Serializable)
                context.startActivity(intent)
            }
            itemView.tv_description.text = delivery.description
            itemView.tv_address.text = delivery.location?.address

            Glide.with(context).load(delivery.imageUrl).apply(RequestOptions.circleCropTransform()).into(itemView.iv_person_image);

  /*          Glide
                    .with(context)
                    .load(delivery.imageUrl)
                    .apply(RequestOptions().centerCrop())
                    .into(itemView.iv_person_image)*/
        }
    }
}