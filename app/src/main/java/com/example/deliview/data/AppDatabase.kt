package com.example.deliview.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.example.deliview.model.Delivery

@Database(entities = arrayOf(Delivery::class),version = 1)
@TypeConverters( LocationConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun deliveryDao() :DeliveryDataDao
}