package com.example.deliview.data

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.example.deliview.model.Delivery
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface DeliveryDataDao {

    @Query("SELECT * from DeliveryData")
    fun getAllDeliveries(): Single<List<Delivery>>

    @Insert(onConflict = REPLACE)
    fun insertDelivery(deliveryList: List<Delivery>)

    @Query("DELETE FROM DeliveryData")
    fun deleteAllRecords()
}