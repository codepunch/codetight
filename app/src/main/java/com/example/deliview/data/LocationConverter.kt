package com.example.deliview.data

import android.arch.persistence.room.TypeConverter
import com.example.deliview.model.Location
import com.google.gson.Gson
import java.io.Serializable

class LocationConverter :Serializable{

    @TypeConverter
    fun fromLocationToString(location: Location): String {
        val gson = Gson()
        return gson.toJson(location)
    }

    @TypeConverter
    fun getLocationFromString(locationString: String): Location {
        val gson = Gson()
        return gson.fromJson(locationString, Location::class.java)
    }

}