package com.example.deliview.constants

class AppConstants {

    companion object {

        const val BASE_URL = "https://mock-api-mobile.dev.lalamove.com/"
        const val DATABASE_NAME: String = "Delivery.db"
    }
}