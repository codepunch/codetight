package com.example.deliview.application

import android.app.Application
import android.arch.persistence.room.Room
import com.example.deliview.constants.AppConstants
import com.example.deliview.data.AppDatabase
import com.example.deliview.network.ApiClient
import com.example.deliview.network.ApiService
import com.facebook.stetho.Stetho

class DeliViewApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        DeliViewApplication.db = Room.databaseBuilder(this.applicationContext, AppDatabase::class.java, AppConstants.DATABASE_NAME).build()
        apiService = ApiClient.getClient().create(ApiService::class.java)
    }

    companion object {
        private lateinit var db: AppDatabase
        private lateinit var apiService: ApiService


        fun getDatabaseInstance(): AppDatabase {
            return db
        }

        fun getRetrofitInstance(): ApiService {
            return apiService
        }
    }

}