
Steps to obtain Google maps API KEY

1) Go to "https://cloud.google.com/" 
2) If you have an account then sign in else sign up.
3) Once logged in click on the console button which is on the right top.
4) There's a dropdown on the top, click to see your projects, if you don't have any just
   create a new one.
5) Then go to project settings, API's enable Maps SDK Andriod
6)After that go to credentials menu. Click on create credentials.
7) Click on create API Key and copy the generated key.(The key will start as "AIza")
8) Add that key to your strings.xml or create a separate xml file to store the key.
9) Apply the key to the project by adding a tag in AndroidManifest file as below.
		  <meta-data
            android:name="com.google.android.geo.API_KEY"
            android:value="Your API Key belongs here" />
10) Done! :)